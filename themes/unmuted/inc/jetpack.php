<?php
/**
 * Jetpack Compatibility File.
 *
 * @link https://jetpack.me/
 *
 * @package Unmuted
 */

/**
 * Add theme support for Infinite Scroll.
 * See: https://jetpack.me/support/infinite-scroll/
 */
function unmuted_jetpack_setup() {
	add_theme_support( 'infinite-scroll', array(
		'container' => 'main',
		'render'    => 'unmuted_infinite_scroll_render',
		'footer'    => 'page',
	) );
} // end function unmuted_jetpack_setup
add_action( 'after_setup_theme', 'unmuted_jetpack_setup' );

/**
 * Custom render function for Infinite Scroll.
 */
function unmuted_infinite_scroll_render() {
	while ( have_posts() ) {
		the_post();
		get_template_part( 'template-parts/content', get_post_format() );
	}
} // end function unmuted_infinite_scroll_render
