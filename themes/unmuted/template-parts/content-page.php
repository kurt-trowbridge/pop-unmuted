<?php
/**
 * Template part for displaying page content in page.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Unmuted
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<div class="post-content">

			<header class="entry-header">
				<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
			</header><!-- .entry-header -->

			<hr class="hr-unmuted">

			<div class="entry-content">
				<?php the_content(); ?>
				<?php
					wp_link_pages( array(
						'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'unmuted' ),
						'after'  => '</div>',
					) );
				?>
			</div><!-- .entry-content -->
		</div><!-- .post-content -->

	<footer class="entry-footer">
		<?php edit_post_link( esc_html__( 'Edit', 'unmuted' ), '<span class="edit-link">', '</span>' ); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->

