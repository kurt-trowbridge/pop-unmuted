<?php
/**
 * Template part for displaying single posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Unmuted
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<?php if ( has_post_thumbnail() ) { ?>
		<div class="post-thumb">
			<?php $image_src = wp_get_attachment_image_src( get_post_thumbnail_id(),'full' );
		  	echo '<img src="' . $image_src[0]  . '" />'; ?>
		</div>
	<?php } ?>

	<div class="post-content">
		<header class="entry-header">
			
			<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>

			<?php if ( 'episodes' === get_post_type() ) { ?>
				<div class="entry-meta">
					<?php unmuted_posted_on(); ?>
				</div><!-- .entry-meta -->
			<?php } ?>

			<?php if ( 'songs' === get_post_type() ) { ?>
				<h2 class="meta-artist"><?php the_field('artist'); ?></h2>
			<?php } ?>			
		</header><!-- .entry-header -->

		<hr class="hr-unmuted">

		<div class="entry-content">
			<?php the_content(); ?>
			<?php
				wp_link_pages( array(
					'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'unmuted' ),
					'after'  => '</div>',
				) );
			?>
		</div><!-- .entry-content -->

<?php
// Find connected pages
$connected = new WP_Query( array(
  'connected_type' => 'people_to_episodes',
  'connected_items' => get_queried_object(),
  'nopaging' => true,
) );

// Display connected pages
if ( $connected->have_posts() ) :
?>
<?php if ( 'episodes' === get_post_type() ) { ?>
	<h3>People in this episode:</h3>
	<div class="people-list">
	<?php while ( $connected->have_posts() ) : $connected->the_post(); ?>
		<div class="bio-block">
		    <?php if ( has_post_thumbnail() ) { ?>
				<!-- <div class="post-thumb">
					<?php $image_src = wp_get_attachment_image_src( get_post_thumbnail_id(),'full' );
				  	echo '<img src="' . $image_src[0]  . '" />'; ?>
				</div> -->
			<?php } ?>
			<div class="bio-content">
			    <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
				<?php if(get_field('location')) : ?>
					<!-- <div><?php the_field('location'); ?></div> -->
				<?php endif; ?>
				<?php the_content(); ?>
			</div>
		</div>
	<?php endwhile; ?>
	</div>
<?php } ?>

<?php 
// Prevent weirdness
wp_reset_postdata();

endif;
?>

<?php
// Find connected pages
$connected = new WP_Query( array(
  'connected_type' => 'songs_to_episodes',
  'connected_items' => get_queried_object(),
  'nopaging' => true,
) );

// Display connected pages
if ( $connected->have_posts() ) :
?>
<?php if ( 'episodes' === get_post_type() ) { ?>
	<h3>Songs discussed in this episode:</h3>
	<div class="people-list">
	<?php while ( $connected->have_posts() ) : $connected->the_post(); ?>
		<div class="bio-block">
		    <?php if ( has_post_thumbnail() ) { ?>
				<div class="post-thumb">
					<?php $image_src = wp_get_attachment_image_src( get_post_thumbnail_id(),'full' );
				  	echo '<img src="' . $image_src[0]  . '" />'; ?>
				</div>
			<?php } ?>
			<div class="bio-content">
			    <h4><a href="<?php the_permalink(); ?>"><?php if(get_field('artist')) : ?><?php the_field('artist'); ?> - <?php endif; ?>"<?php the_title(); ?>"</a></h4>
				<?php the_content(); ?>
			</div>
		</div>
	<?php endwhile; ?>
	</div>
<?php } ?>

<?php 
// Prevent weirdness
wp_reset_postdata();

endif;
?>

<?php
// Find connected pages
$connected = new WP_Query( array(
  'connected_type' => 'people_to_episodes',
  'connected_items' => get_queried_object(),
  'nopaging' => true,
) );

// Display connected pages
if ( $connected->have_posts() ) :
?>
<?php if ( 'people' === get_post_type() ) { ?>
	<h3>On these episodes:</h3>
	<div class="people-list">
	<?php while ( $connected->have_posts() ) : $connected->the_post(); ?>
		<div class="bio-block">
		    <?php if ( has_post_thumbnail() ) { ?>
				<div class="post-thumb">
					<?php $image_src = wp_get_attachment_image_src( get_post_thumbnail_id(),'full' );
				  	echo '<img src="' . $image_src[0]  . '" />'; ?>
				</div>
			<?php } ?>
			<div class="bio-content">
			    <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
			    <div class="entry-summary">
    				<?php the_excerpt(); ?>
    			</div><!-- .entry-summary -->
			</div>
		</div>
	<?php endwhile; ?>
	</div>
<?php } ?>

<?php 
// Prevent weirdness
wp_reset_postdata();

endif;
?>

<h3>Listen and subscribe:</h3>
<a href="<?php the_field('itunes_link'); ?>">iTunes</a>
<a href="<?php the_field('stitcher_link'); ?>">Stitcher</a>

<?php
// Find connected pages
$connected = new WP_Query( array(
  'connected_type' => 'songs_to_episodes',
  'connected_items' => get_queried_object(),
  'nopaging' => true,
) );

// Display connected pages
if ( $connected->have_posts() ) :
?>
<?php if ( 'songs' === get_post_type() ) { ?>
	<h3>Discussed in these episodes:</h3>
	<div class="people-list">
	<?php while ( $connected->have_posts() ) : $connected->the_post(); ?>
		<div class="bio-block">
		    <?php if ( has_post_thumbnail() ) { ?>
				<div class="post-thumb">
					<?php $image_src = wp_get_attachment_image_src( get_post_thumbnail_id(),'full' );
				  	echo '<img src="' . $image_src[0]  . '" />'; ?>
				</div>
			<?php } ?>
			<div class="bio-content">
			    <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
			    <div class="entry-summary">
    				<?php the_excerpt(); ?>
    			</div><!-- .entry-summary -->
			</div>
		</div>
	<?php endwhile; ?>
	</div>
<?php } ?>

<?php 
// Prevent weirdness
wp_reset_postdata();

endif;
?>

<?php
// Find connected pages
$connected = new WP_Query( array(
  'connected_type' => 'songs_to_episodes',
  'connected_items' => get_queried_object(),
  'nopaging' => true,
) );

// Display connected pages
if ( $connected->have_posts() ) :
?>
<?php if ( 'people' === get_post_type() ) { ?>
<h3>Songs in this episode:</h3>
<ul>
<?php while ( $connected->have_posts() ) : $connected->the_post(); ?>
    <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
<?php endwhile; ?>
</ul>
<?php } ?>

<?php 
// Prevent weirdness
wp_reset_postdata();

endif;
?>

		<footer class="entry-footer">
			<?php unmuted_entry_footer(); ?>
		</footer><!-- .entry-footer -->
	</div>
</article><!-- #post-## -->

