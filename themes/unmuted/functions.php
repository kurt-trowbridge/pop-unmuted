<?php
/**
 * Unmuted functions and definitions.
 *
 * @link https://codex.wordpress.org/Functions_File_Explained
 *
 * @package Unmuted
 */

if ( ! function_exists( 'unmuted_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function unmuted_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on Unmuted, use a find and replace
	 * to change 'unmuted' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'unmuted', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary Menu', 'unmuted' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See https://developer.wordpress.org/themes/functionality/post-formats/
	 */
	//add_theme_support( 'post-formats', array(
	//	'aside',
	//	'image',
	//	'video',
	//	'quote',
	//	'link',
	//) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'unmuted_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif; // unmuted_setup
add_action( 'after_setup_theme', 'unmuted_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function unmuted_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'unmuted_content_width', 640 );
}
add_action( 'after_setup_theme', 'unmuted_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function unmuted_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'unmuted' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'unmuted_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function unmuted_scripts() {
	wp_enqueue_style( 'unmuted-style', get_stylesheet_uri() );

	wp_enqueue_script( 'unmuted-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20120206', true );

	wp_enqueue_script( 'unmuted-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );

	wp_enqueue_script( 'unmuted-emotion-font', get_template_directory_uri() . '/js/emotion-font.js', array(), '20160830', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'unmuted_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';


/*** Kurt's additions ***/

/**
 * Posts 2 Posts connections.
 */
function my_connection_types() {
    p2p_register_connection_type( array(
        'name' => 'people_to_episodes',
        'from' => 'people',
        'to' => 'episodes'
    ) );
    p2p_register_connection_type( array(
        'name' => 'people_to_songs',
        'from' => 'people',
        'to' => 'songs'
    ) );
    p2p_register_connection_type( array(
        'name' => 'songs_to_episodes',
        'from' => 'songs',
        'to' => 'episodes'
    ) );
}
add_action( 'p2p_init', 'my_connection_types' );

/**
 * Add Episodes to latest posts loops
 */
add_filter( 'pre_get_posts', 'my_get_posts' );

function my_get_posts( $query ) {

	if ( is_home() && $query->is_main_query() )
		$query->set( 'post_type', array( 'post', 'episodes' ) );

	return $query;
}

function myfeed_request($qv) {
	if (isset($qv['feed']) && !isset($qv['post_type']))
		$qv['post_type'] = array('post', 'episodes', 'kpop', 'crowncast');
	return $qv;
}
add_filter('request', 'myfeed_request');