<?php
/*
Plugin Name: Songs Custom Post Type
Plugin URI: http://kurttrowbridge.com
Description: Declares a plugin that will create a custom post type housing songs.
Version: 1.0.4
Author: Kurt Trowbridge
Author URI: http://kurttrowbridge.com
License: GPLv2
*/

add_action( 'init', 'create_song' );

function create_song() {
    register_post_type( 'songs',
        array(
            'labels' => array(
                'name' => 'Songs',
                'singular_name' => 'Song',
                'add_new' => 'Add New',
                'add_new_item' => 'Add New Song',
                'edit' => 'Edit',
                'edit_item' => 'Edit Song',
                'new_item' => 'New Song',
                'view' => 'View',
                'view_item' => 'View Song',
                'search_items' => 'Search Songs',
                'not_found' => 'No Songs found',
                'not_found_in_trash' => 'No Songs found in Trash',
                'parent' => 'Parent Song'
            ),
 
            'public' => true,
            'menu_position' => 17,
			'menu_icon' => 'dashicons-controls-play',
            'supports' => array( 'title', 'thumbnail'),
            'taxonomies' => array( '' ),
            'has_archive' => true
        )
    );
}