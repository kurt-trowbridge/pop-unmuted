<?php
/*
Plugin Name: Episodes Custom Post Type
Plugin URI: http://kurttrowbridge.com
Description: Declares a plugin that will create a custom post type housing episodes.
Version: 1.0.4
Author: Kurt Trowbridge
Author URI: http://kurttrowbridge.com
License: GPLv2
*/

add_action( 'init', 'create_episode' );

function create_episode() {
    register_post_type( 'episodes',
        array(
            'labels' => array(
                'name' => 'Episodes',
                'singular_name' => 'Episode',
                'add_new' => 'Add New',
                'add_new_item' => 'Add New Episode',
                'edit' => 'Edit',
                'edit_item' => 'Edit Episode',
                'new_item' => 'New Episode',
                'view' => 'View',
                'view_item' => 'View Episode',
                'search_items' => 'Search Episodes',
                'not_found' => 'No Episodes found',
                'not_found_in_trash' => 'No Episodes found in Trash',
                'parent' => 'Parent Episode'
            ),
 
            'public' => true,
            'menu_position' => 15,
			'menu_icon' => 'dashicons-format-audio',
            'supports' => array( 'title', 'editor', 'comments', 'thumbnail', 'excerpt'),
            'taxonomies' => array( 'post_tag' ),
            'has_archive' => true
        )
    );
}