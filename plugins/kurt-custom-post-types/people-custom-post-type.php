<?php
/*
Plugin Name: People Custom Post Type
Plugin URI: http://kurttrowbridge.com
Description: Declares a plugin that will create a custom post type housing people.
Version: 1.0.0
Author: Kurt Trowbridge
Author URI: http://kurttrowbridge.com
License: GPLv2
*/

add_action( 'init', 'create_person' );

function create_person() {
    register_post_type( 'people',
        array(
            'labels' => array(
                'name' => 'People',
                'singular_name' => 'Person',
                'add_new' => 'Add New',
                'add_new_item' => 'Add New Person',
                'edit' => 'Edit',
                'edit_item' => 'Edit Person',
                'new_item' => 'New Person',
                'view' => 'View',
                'view_item' => 'View Person',
                'search_items' => 'Search People',
                'not_found' => 'No People found',
                'not_found_in_trash' => 'No People found in Trash',
                'parent' => 'Parent Person'
            ),
 
            'public' => true,
            'menu_position' => 16,
			'menu_icon' => 'dashicons-admin-users',
            'supports' => array( 'title', 'editor', 'comments', 'thumbnail'),
            'taxonomies' => array( '' ),
            'has_archive' => true
        )
    );
}