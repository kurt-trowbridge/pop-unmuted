<?php
/*
Plugin Name: K-Pop Episodes Custom Post Type
Plugin URI: http://kurttrowbridge.com
Description: Declares a plugin that will create a custom post type housing K-Pop Unmuted episodes.
Version: 1.0.0
Author: Kurt Trowbridge
Author URI: http://kurttrowbridge.com
License: GPLv2
*/

add_action( 'init', 'create_kpop_episode' );

function create_kpop_episode() {
    register_post_type( 'kpop',
        array(
            'labels' => array(
                'name' => 'K-Pop Episodes',
                'singular_name' => 'K-Pop Episode',
                'add_new' => 'Add New',
                'add_new_item' => 'Add New K-Pop Episode',
                'edit' => 'Edit',
                'edit_item' => 'Edit K-Pop Episode',
                'new_item' => 'New K-Pop Episode',
                'view' => 'View',
                'view_item' => 'View K-Pop Episode',
                'search_items' => 'Search K-Pop Episodes',
                'not_found' => 'No K-Pop Episodes found',
                'not_found_in_trash' => 'No K-Pop Episodes found in Trash',
                'parent' => 'Parent K-Pop Episode'
            ),
 
            'public' => true,
            'menu_position' => 16,
			'menu_icon' => 'dashicons-format-audio',
            'supports' => array( 'title', 'editor', 'comments', 'thumbnail', 'excerpt'),
            'taxonomies' => array( 'post_tag' ),
            'has_archive' => true
        )
    );
}