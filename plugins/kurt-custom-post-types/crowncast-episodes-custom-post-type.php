<?php
/*
Plugin Name: Crowncast Episodes Custom Post Type
Plugin URI: http://kurttrowbridge.com
Description: Declares a plugin that will create a custom post type housing Crowncast episodes.
Version: 1.0.0
Author: Kurt Trowbridge
Author URI: http://kurttrowbridge.com
License: GPLv2
*/

add_action( 'init', 'create_crowncast_episode' );

function create_crowncast_episode() {
    register_post_type( 'crowncast',
        array(
            'labels' => array(
                'name' => 'Crowncast Episodes',
                'singular_name' => 'Crowncast Episode',
                'add_new' => 'Add New',
                'add_new_item' => 'Add New Crowncast Episode',
                'edit' => 'Edit',
                'edit_item' => 'Edit Crowncast Episode',
                'new_item' => 'New Crowncast Episode',
                'view' => 'View',
                'view_item' => 'View Crowncast Episode',
                'search_items' => 'Search Crowncast Episodes',
                'not_found' => 'No Crowncast Episodes found',
                'not_found_in_trash' => 'No Crowncast Episodes found in Trash',
                'parent' => 'Parent Crowncast Episode'
            ),
 
            'public' => true,
            'menu_position' => 16,
			'menu_icon' => 'dashicons-format-audio',
            'supports' => array( 'title', 'editor', 'comments', 'thumbnail', 'excerpt'),
            'taxonomies' => array( 'post_tag' ),
            'has_archive' => true
        )
    );
}